export const getPharmacies = token => {
    console.log(token)
    let tokenAuth = token.token
    let header = new Headers()
    header.append('Authorization', 'Bearer ' +tokenAuth)
    header.append('Accept', 'application/json')
    let BaseUri = 'http://localhost/public/api/pharmacies';
    let req = new Request(BaseUri,{
        method: 'GET',
        headers: header,
        mode: 'cors'
    });

    return fetch(req)
        .then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            console.log(error);
        })
}

export const getPharmacy = ({ token, criteria }) => {
    let tokenAuth = token
    let header = new Headers()
    header.append('Authorization', 'Bearer ' +tokenAuth)
    header.append('Accept', 'application/json')
    let BaseUri = 'http://localhost/public/api/pharmacies?criteria='+criteria;
    let req = new Request(BaseUri,{
        method: 'GET',
        headers: header,
        mode: 'cors'
    });

    return fetch(req)
        .then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            console.log(error);
        })
}