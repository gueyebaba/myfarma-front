import React, {Component} from 'react';

class FooterPage extends Component{

    constructor(props) {
        super(props);
        this.state = {
        }

    }

    render() {

        return (
            <div className="FooterPage">
                <section id="contact" data-stellar-background-ratio="0.5">
                    <div className="container">
                        <div className="row">

                            <div className="col-md-12 col-sm-12">
                                <div className="section-title">
                                    <h2>Contact us</h2>
                                    <span className="line-bar">...</span>
                                </div>
                            </div>

                            <div className="col-md-8 col-sm-8">
                                <form id="contact-form"  action="#" method="post">
                                    <div className="col-md-6 col-sm-6">
                                        <input type="text" className="form-control" placeholder="Full Name" id="cf-name"
                                               name="cf-name" required="" />
                                    </div>

                                    <div className="col-md-6 col-sm-6">
                                        <input type="email" className="form-control" placeholder="Your Email"
                                               id="cf-email" name="cf-email" required="" />
                                    </div>

                                    <div className="col-md-6 col-sm-6">
                                        <input type="tel" className="form-control" placeholder="Your Phone"
                                               id="cf-number" name="cf-number" required="" />
                                    </div>

                                    <div className="col-md-6 col-sm-6">
                                        <select className="form-control" id="cf-budgets" name="cf-budgets">
                                            <option>Budget Level</option>
                                            <option>$500 to $1,000</option>
                                            <option>$1,000 to $2,200</option>
                                            <option>$2,200 to $4,500</option>
                                            <option>$4,500 to $7,500</option>
                                            <option>$7,500 to $12,000</option>
                                            <option>$12,000 or more</option>
                                        </select>
                                    </div>
                                    <div className="col-md-12 col-sm-12">
                                        <textarea className="form-control" rows="6" placeholder="Your requirements"
                                                  id="cf-message" name="cf-message" required=""></textarea>
                                    </div>
                                    <div className="col-md-4 col-sm-12">
                                        <input type="submit" className="form-control" name="submit"
                                               value="Send Message" />
                                    </div>
                                </form>
                            </div>
                            <div className="col-md-4 col-sm-4">
                                <div className="google-map">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3647.3030413476204!2d100.5641230193719!3d13.757206847615207!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf51ce6427b7918fc!2sG+Tower!5e0!3m2!1sen!2sth!4v1510722015945"
                                        allowFullScreen
                                        title="myFrame">

                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <footer data-stellar-background-ratio="0.5">
                    <div className="container">
                        <div className="row">

                            <div className="col-md-5 col-sm-12">
                                <div className="footer-thumb footer-info">
                                    <h2>Hydro Company</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                            </div>

                            <div className="col-md-2 col-sm-4">
                                <div className="footer-thumb">
                                    <h2>Company</h2>
                                    <ul className="footer-link">
                                        <li><a href="#about">About Us</a></li>
                                        <li><a href="#join">Join our team</a></li>
                                        <li><a href="#read">Read Blog</a></li>
                                        <li><a href="#press">Press</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-md-2 col-sm-4">
                                <div className="footer-thumb">
                                    <h2>Services</h2>
                                    <ul className="footer-link">
                                        <li><a href="#pricing">Pricing</a></li>
                                        <li><a href="#doc">Documentation</a></li>
                                        <li><a href="#support">Support</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-md-3 col-sm-4">
                                <div className="footer-thumb">
                                    <h2>Find us</h2>
                                    <p>123 Grand Rama IX,  Krung Thep Maha Nakhon 10400
                                    </p>
                                </div>
                            </div>

                            <div className="col-md-12 col-sm-12">
                                <div className="footer-bottom">
                                    <div className="col-md-6 col-sm-5">
                                        <div className="copyright-text">
                                            <p>Copyright &copy; 2017 Your Company</p>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-sm-7">
                                        <div className="phone-contact">
                                            <p>Call us <span>(+66) 010-020-0340</span></p>
                                        </div>
                                        <ul className="social-icon">
                                            <li><a href="#facebook" className="fa fa-facebook-square">{}</a></li>
                                            <li><a href="#twitter" className="fa fa-twitter">{}</a></li>
                                            <li>
                                                <a href="#instagram" className="fa fa-instagram">{}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default FooterPage;
