import React, {Component} from 'react';
import {getPharmacies} from "../../Services/Pharmacies";
import {getPharmacy} from "../../Services/Pharmacies";
import {getToken} from "../../Services/Authentification";

class ContentPage extends Component{
    state = {
        pharmacies: {},
        criteria: '',
        token: ''
    }

    async componentDidMount() {
        const token = await getToken();
        localStorage.setItem('token', token.token)
        const pharmacies = await getPharmacies(token);
        this.setState({
            pharmacies,
            token: token.token
        });
    }

    handleSubmit = event => {
        event.preventDefault()
        let token = this.state.token
        let criteria = this.state.criteria
        const pharmacy = getPharmacy({token,criteria});
        pharmacy.then(function(result) {
           return result
        }).then((response) => {
            this.setState({ pharmacies: response });
        });
    }

    handleChange = event => {
        let criteria = event.target.value
        this.setState({
            criteria
        })
    }

    render() {
      const { pharmacies } = this.state;
      return (
            <div className="ContentPage">
                <section id="home" data-stellar-background-ratio="0.5">
                    <div className="container">
                        <form
                            onSubmit={this.handleSubmit}
                        >
                            <div className="row">
                                <div className="col-sm-8">
                                    <div className="input-group">
                                        <input
                                            size="125"
                                            className="form-control border-secondary py-2" type="search"
                                            value={this.state.search}
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <button className="btn btn-outline-secondary" type="submit">
                                        <i className="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                <section id="about" data-stellar-background-ratio="0.5">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-5 col-sm-6">
                                <div className="about-info">
                                    <div className="section-title">
                                        <span className="line-bar">...</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor.</p>
                                </div>
                            </div>

                            <div className="col-md-3 col-sm-6">
                                <div className="about-info skill-thumb">
                                    {
                                        pharmacies.items && pharmacies.items.map((res, index) => (
                                            <div key={index}>
                                                <h3> {res.generated_name} </h3>
                                                <strong> {res.address.street_number} , {res.address.street_name}  {res.address.city}</strong>
                                                <div className="progress">
                                                    <div className="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        ))
                                    }
                                </div>
                                </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default ContentPage;


