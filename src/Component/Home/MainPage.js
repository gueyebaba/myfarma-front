import React, {Component} from 'react';
import  HeaderPage from './HeaderPage'
import  ContentPage from './ContentPage'
import  FooterPage from './FooterPage'

class MainPage extends Component{


    render() {
        return (
            <div className="MainPage">
                <HeaderPage />
                <ContentPage />
                <FooterPage />
            </div>
        );
    }
}

export default MainPage;
