import React, {Component} from 'react';

class HeaderPage extends Component{

    constructor(props) {
        super(props);
        this.state = {
        }

    }
    render() {
        return (
            <div >
                <section className="navbar custom-navbar navbar-fixed-top" role="navigation">
                    <div className="container">
                        <div className="navbar-header">
                            <button className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span className="icon icon-bar"></span>
                                <span className="icon icon-bar"></span>
                                <span className="icon icon-bar"></span>
                            </button>
                            <a href="index.html" className="navbar-brand">MyFarma</a>
                        </div>

                        <div className="collapse navbar-collapse">
                            <ul className="nav navbar-nav navbar-nav-first">
                                <li><a href="#home" className="smoothScroll">Home</a></li>
                                <li><a href="#about" className="smoothScroll">About</a></li>
                                <li><a href="#blog" className="smoothScroll">Blog</a></li>
                                <li><a href="#work" className="smoothScroll">Our Work</a></li>
                                <li><a href="#contact" className="smoothScroll">Contacts</a></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <li><a href="#facebook"><i className="fa fa-facebook-square"></i></a></li>
                                <li><a href="#twitter"><i className="fa fa-twitter"></i></a></li>
                                <li><a href="#insta"><i className="fa fa-instagram"></i></a></li>
                                <li className="section-btn">
                                    <a href="#modal" data-toggle="modal" data-target="#modal-form">Sign in / Join</a></li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default HeaderPage;
