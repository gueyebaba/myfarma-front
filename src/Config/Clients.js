import React, {Component} from 'react';
import MainPage from '../Component/Home/MainPage'
import PharmacyRequest from '../Networking/PharmacyRequest'

class Clients extends Component{
  render() {
    return (
        <div className="App">
          <MainPage />
          <PharmacyRequest />
        </div>
    );
  }
}

export default Clients;
